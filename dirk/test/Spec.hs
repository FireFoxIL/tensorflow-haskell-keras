{-# LANGUAGE DataKinds, ScopedTypeVariables, TypeOperators, RankNTypes, FlexibleContexts  #-}

module Main where

import Dirk.Capabilities
import Dirk.Data
import Dirk.Native

import Test.HUnit (assertEqual)
import Test.HUnit.Approx (assertApproxEqual)
import Test.Framework (defaultMain, Test)
import Test.Framework.Providers.HUnit (testCase)

modelAdd :: Add m (Double # 2 # 3) => m (Double # 2 # 3) -> m (Double # 2 # 3) -> m (Double # 2 # 3)
modelAdd = add
testAdd :: Test
testAdd = testCase "Add model" (assertEqual "Result is correct" (interpret result) [2, 4, 6, 8, 10, 12])
  where
    modelNative :: Ctx (Double # 2 # 3) -> Ctx (Double # 2 # 3) -> Ctx (Double # 2 # 3) = modelAdd
    input :: Ctx (Double # 2 # 3) = lift [1, 2, 3, 4, 5, 6]
    result :: Ctx (Double # 2 # 3) = modelNative input input

main :: IO ()
main = defaultMain $ [testAdd]
