{-|
This module defines implementations of builders and operations
in the native haskell environment
-}
module Dirk.Native(
  Ctx(..), CtxF,
) where

import Data.Functor.Identity (Identity(Identity))
import Dirk.Data
import Dirk.Capabilities

-- Context

-- | Building tensors
-- | For specific backend context
type family CtxF (a :: Type) where
  CtxF () = ()
  CtxF (b # i) = [Inner b]
  CtxF Double = Double

-- | Synonim of context with built tensor
newtype Ctx a = Ctx (CtxF a)

-- Operators

-- | Instance of element-wise add operation for adding vectors
-- | with native lists 
instance (Num x, Scalar x) => Add Ctx (x # i) where
  add (Ctx a) (Ctx b) = Ctx (zipWith (+) a b)

-- | Instance of element-wise add operation
-- | for recursie adding tensors 
instance {-# OVERLAPPABLE #-} Add Ctx (x # i) => Add Ctx (x # i # j) where
  -- The instances are not overlapping due to the `Scalar` constraint on the initial instance
  add (Ctx a) (Ctx b) = Ctx inter
    where Ctx inter = add (Ctx @(x # i) a) (Ctx @(x # i) b)


-- Builders

-- | Lift empty list to the context
instance PureBuilder () Ctx () where
  lift = Ctx

-- | Lift a double to the context
instance PureBuilder Double Ctx Double where
  lift = Ctx

-- | Lift a scalar to the context
instance Scalar x => PureBuilder [x] Ctx (x # i) where
  lift = Ctx

-- | Lift a tensor to the context
instance (PureBuilder [y] Ctx (x # i), y ~ Inner x) => PureBuilder [y] Ctx (x # i # j) where
  lift = Ctx

-- Interpreters

-- | Interpret a double in a context as a pure double variable 
instance Interpreter Ctx Double where
  type Result Ctx Double = Double
  interpret (Ctx a) = a

-- | Interpret a tensor in a context to a list
instance Interpreter Ctx (x # n) where
  type Result Ctx (x # n) = [Inner x]
  interpret (Ctx a) = a
