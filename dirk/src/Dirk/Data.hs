{-# LANGUAGE AllowAmbiguousTypes #-}

module Dirk.Data
  ( type (#) (..),
    Inner,
    Scalar,
    PureBuilder(..),
    Interpreter(..),
    ExtractShape(..),
    WithSession(..),
    Type,
    Nat,
    KnownNat
  )
where

import Data.Kind (Type)
import Data.Proxy
import GHC.TypeLits (Nat, KnownNat, natVal)

-- | Type for a tensor with encoded type and shape 
data (t :: Type) # (n :: Nat)

-- | Unwrap # data type to t
-- | Can be considered as discarding
-- | last dimension of a tensor  tensor
type family Inner (a :: Type) where
  Inner (t # i) = Inner t
  Inner t = t

-- | Scalar equals to tensor with shape 1
type Scalar x = x ~ Inner x

infixl 7 #

-- | Lift a value a  to the context m
class PureBuilder input m a where
  lift :: input -> m a

-- | Interpret 
class Interpreter m a where
  type Result m a
  interpret :: m a -> Result m a

-- | Abstract session that need to be instantiated
-- | for each backend individually
class (Monad g) => WithSession m a g b where
  session :: m a -> g b
  run :: g b -> IO b

-- | Get the shape of a tensor x in a context m
class ExtractShape m x where
  getShape :: forall proxy. proxy (m x) -> [Integer]
