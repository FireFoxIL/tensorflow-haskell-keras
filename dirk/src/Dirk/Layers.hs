{-|
This module defines basic layers and associated opertaions
-}
module Dirk.Layers (
  Module(..),
  dense,
  compose,
  activation,
  Trainable(..),
  DenseParameters(..),
  mse,
  inference,
) where

import Dirk.Data
import Dirk.Capabilities

-- | Module represents a combination of layers
-- | with parameters and forward pass function
data Module m inp out prm = Module {
  forward :: prm -> m inp -> m out,
  parameters :: prm
}

-- | Parameters of a dense layer
data DenseParameters m x inp out = DenseParameters {
  weights :: m (x # inp # out),
  bias :: m (x # out)
}

-- | Is model supposed to be trained
class (Monad g) => Trainable m x inp out params g  where
  -- | 
  trainStep :: Module m (x # inp) (x # out) params
               -> (m (x # out) -> m (x # out) -> m (x # out))
               -> m (x # inp)
               -> m (x # out) -> g ()

-- | Concatenates two layers a one module
compose ::
  Module m inp middle prm1 ->
  Module m middle out prm2 ->
  Module m inp out (prm1, prm2)
compose l r = Module {
  forward = \params -> (forward r (snd params)) . (forward l (fst params)),
  parameters = (parameters l, parameters r)
}

-- | Sigmoid function as a separate layer
activation :: Sigmoid m x => Module m x x ()
activation = Module {
  forward = \_ -> \x -> sigmoid x,
  parameters = ()
}

-- | Dense layer 
dense ::
  forall (inp :: Nat) (out :: Nat) m x y.
  (Builder m (y # inp # out),
  Builder m (y # out),
  MatMultiply m x y,
  AddN m x y) =>
  Module m (x # inp) (x # out) (DenseParameters m y inp out)
dense =  Module {
  forward = expression,
  parameters = ps
} where
  expression params = \inp -> (inp `matMul` w) `addN` b where
    w = weights params
    b = bias params

  ps = DenseParameters {
    weights = build,
    bias = build
  }

-- TODO: add mean
-- | Mean Squared Error between 2 tensors of the same shape
mse :: forall m x.
  (Multiply m x,
  Subtract m x) =>
  m x -> m x -> m x
mse a b = res `mul` res where
  res = a `sub` b
  
-- | Returns a session to run for performing forward pass on a module
inference :: forall m x (inp :: Nat) (out :: Nat) g b moduleParams. WithSession m (x # out) g b =>
             Module m (x # inp) (x # out) moduleParams -> m (x # inp) -> g b
inference mod input = session (forward mod (parameters mod) input)

