{-|
This module defines possible operations as type classes 
to be to be instantiated for each specific backend
-}
module Dirk.Capabilities
  ( Add (..),
    AddN (..),
    Subtract (..),
    Multiply (..),
    Divide (..),
    MatMultiply (..),
    Convert (..),
    Sigmoid (..),
    Builder (..),
  )
where

import Dirk.Data

-- | Addition of elements of type x in a context m.
-- | Can be considered as an element-wise addition
-- | of two tensors of the same shape
class Add m x where
  add :: m x -> m x -> m x

class (Inner x ~ y) => AddN m x y where
  addN :: forall (j :: Nat) . m (x # j) -> m (y # j) -> m (x # j)

-- | Substraction of elements of type x in a context m.
-- | Can be considered as an element-wise substraction
-- | of two tensors of the same shape
class Subtract m x where
  sub :: m x -> m x -> m x

-- | Multiplication of elements of type x in a context m.
-- | Can be considered as an element-wise multiplication
-- | of two tensors of the same shape
class Multiply m x where
  mul :: m x -> m x -> m x

-- | Division of elements of type x in a context m.
-- | Can be considered as an element-wise division
-- | of two tensors of the same shape
class Divide m x where
  div :: m x -> m x -> m x

-- | Multiplication of elements of types (x # j) and (y # j # k)
-- | in a context m.
-- | Can be considered as a matrix multiplication
-- | of two tensors with shapes differing by no more than one dimension
class (Inner x ~ y) => MatMultiply m x y where
  matMul :: forall (j :: Nat) (k :: Nat). m (x # j) -> m (y # j # k) -> m (x # k)

-- | Convert an element x in context m
-- | to y in context m.
-- | Can be considered as reshaping of a tensor.
class Convert m x y where
  convert :: m x -> m y


-- | Apply sigmoid function to x in context m
-- | Can be considered as element-wise 
-- | sigmoid application to a tensor  
class Sigmoid m x where -- TODO: change to a constrained functor definition
  sigmoid :: m x -> m x

-- | Create x in a context m
-- | Can be used for creating tensor of weights
class Builder m x where
  build :: m x
