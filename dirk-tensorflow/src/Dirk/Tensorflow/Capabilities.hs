{-# LANGUAGE UndecidableInstances, InstanceSigs #-}

module Dirk.TensorFlow.Capabilities where

import Data.Int (Int32, Int64)
import Data.Complex (Complex)

import Dirk.Data
import Dirk.Capabilities
import Dirk.TensorFlow.Core

import qualified TensorFlow.Ops as TF
import qualified TensorFlow.Core as TF

-- Operations

-- | Implementation of an element-wise addition 
-- | of two double vectors of the same shape 
instance Add TFCtx (Float # i) where
  add (TFCtx a) (TFCtx b) = TFCtx (a `TF.add` b)

-- | Implementation of a recursive element-wise addition 
-- | of two double tensors of the same shape  
instance {-# OVERLAPPABLE #-} Add TFCtx (x # i) => Add TFCtx (x # i # j) where
  -- ^ The instances are not overlapping due to the `Scalar` constraint on the initial instance
  add (TFCtx a) (TFCtx b) = TFCtx inter
    where TFCtx inter = add (TFCtx @(x # i) a) (TFCtx @(x # i) b)

-- | Implementation of an element-wise substraction 
-- | of two double vectors of the same shape 
instance Subtract TFCtx (Float # i) where
  sub (TFCtx a) (TFCtx b) = TFCtx (a `TF.sub` b)

-- | Implementation of a recursive element-wise substraction 
-- | of two double vectors of the same shape 
instance {-# OVERLAPPABLE #-} Subtract TFCtx (x # i) => Subtract TFCtx (x # i # j) where
  -- ^ The instances are not overlapping due to the `Scalar` constraint on the initial instance
  sub (TFCtx a) (TFCtx b) = TFCtx inter
    where TFCtx inter = sub (TFCtx @(x # i) a) (TFCtx @(x # i) b)

-- | Implementation of an element-wise multiplication 
-- | of two double vectors of the same shape
instance Multiply TFCtx (Float # i) where
  mul (TFCtx a) (TFCtx b) = TFCtx (a `TF.mul` b)

-- | Implementation of a recursive element-wise multiplication 
-- | of two double tensors of the same shape
instance {-# OVERLAPPABLE #-} Multiply TFCtx (x # i) => Multiply TFCtx (x # i # j) where
  -- ^ The instances are not overlapping due to the `Scalar` constraint on the initial instance
  mul (TFCtx a) (TFCtx b) = TFCtx inter
    where TFCtx inter = mul (TFCtx @(x # i) a) (TFCtx @(x # i) b)

-- | Implementation of a matrix multiplication 
-- | of vector and matrix
instance MatMultiply TFCtx Float Float where
  matMul (TFCtx a) (TFCtx b) = TFCtx (a `TF.matMul` b)

-- | Implementation of a recursive matrix multiplication 
-- | of two double tensors of the same shape
instance {-# OVERLAPPABLE #-} (MatMultiply TFCtx x y)
  => MatMultiply TFCtx (x # m) y where
  -- ^ The instances are not overlapping due to the `Scalar` constraint on the initial instance
  matMul :: forall (j :: Nat) (k :: Nat). TFCtx (x # m # j) -> TFCtx (y # j # k) -> TFCtx (x # m # k)
  matMul (TFCtx a) (TFCtx b) = TFCtx inter
    where TFCtx inter = matMul (TFCtx @(x # j) a) (TFCtx @(y # j # k) b)

-- | Implemetation of applying sigmoid function for vector
instance Sigmoid TFCtx (Float # i) where
  sigmoid (TFCtx a) = TFCtx (TF.sigmoid a)

-- | Implemetation of applying sigmoid function for tensor
instance {-# OVERLAPPABLE #-} Sigmoid TFCtx (x # i) => Sigmoid TFCtx (x # i # j) where
  -- ^ The instances are not overlapping due to the `Scalar` constraint on the initial instance
  sigmoid (TFCtx a) = TFCtx inter
    where TFCtx inter = sigmoid (TFCtx @(x # i) a)

instance AddN TFCtx Float Float where
  addN (TFCtx a) (TFCtx b) = TFCtx (a `TF.add` b)

instance {-# OVERLAPPABLE #-} AddN TFCtx x y => AddN TFCtx (x # m) y where
  addN :: forall (j :: Nat). TFCtx (x # m # j) -> TFCtx (y # j) -> TFCtx (x # m # j)
  addN (TFCtx a) (TFCtx b) = TFCtx inter
    where TFCtx inter = addN (TFCtx @(x # j) a) (TFCtx @(y # j) b)
