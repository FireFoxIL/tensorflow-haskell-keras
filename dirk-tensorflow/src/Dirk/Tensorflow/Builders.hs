{-# LANGUAGE UndecidableInstances #-}
{-|
Module defines context builders and sesson for the tensorflow backend
-}
module Dirk.TensorFlow.Builders where

import Data.Int (Int32, Int64)
import Data.Complex (Complex)
import Data.Random.Normal
import Data.Proxy
import Control.Monad ((>=>))
import GHC.TypeLits (KnownNat(..), natVal, Nat(..))
import System.Random
import qualified Data.Vector as V

import Dirk.TensorFlow.Core
import Dirk.Data
import Dirk.Capabilities (Convert(..), Builder(..))
import Dirk.Layers (Trainable(..), DenseParameters(..), Module(..))

import qualified TensorFlow.Core as TF
import qualified TensorFlow.Ops as TF
import qualified TensorFlow.Minimize as TF
import qualified TensorFlow.Variable as TFV

-- | Lift empty list to the tensorflow context
instance PureBuilder () TFCtx () where
  lift = TFCtx

-- | Lift a float variable list to the tensorflow context
instance PureBuilder Float TFCtx Float where
  lift = TFCtx

-- | Lift a tensor with shape 1 (scalar) to the tensorflow context
instance Convert TFCtx Float (Float # 1) where
  convert (TFCtx sc) = TFCtx (TF.scalar sc)

-- | Get the shape of a vector in the tensorflow context
instance KnownNat i => ExtractShape TFCtx (Float # i) where
  getShape _ = [natVal (Proxy :: Proxy i)]

-- | Recursively get the shape of a tensor in the tensorflow context
instance {-# OVERLAPPABLE #-} (ExtractShape TFCtx (x # i), KnownNat j) => ExtractShape TFCtx (x # i # j) where
  getShape _ = (getShape (Proxy :: Proxy (TFCtx (x # i)))) ++ [natVal (Proxy :: Proxy j)]

-- | Lift a vector to the tensorflow context
instance KnownNat i => PureBuilder [Float] TFCtx (Float # i) where
  lift xs = TFCtx (TF.vector (take size xs)) where
    size = fromIntegral (natVal (Proxy :: Proxy i))

-- | Lift a tensor to the tensorflow context
instance (PureBuilder [Float] TFCtx (x # i),
          Inner x ~ Float,
          ExtractShape TFCtx (x # i # j)) => PureBuilder [Float] TFCtx (x # i # j) where
  lift xs = TFCtx (TF.reshape liftedVector liftedShape) where
    liftedVector = TF.vector (take totalSize xs)
    liftedShape  = TF.vector transformedShape
    totalSize = product (map fromIntegral shape)
    transformedShape :: [Int64] = map fromIntegral shape
    shape = getShape (Proxy :: Proxy (TFCtx (x # i # j)))


stdGen = mkStdGen 1337

-- | Create a random vector in the tensorflow context
-- | for weights initialization
instance KnownNat i => Builder TFCtx (Float # i) where
  build = lift normalDistribution where
    normalDistribution :: [Float] = normals stdGen

-- | Create a random tensor in the tensorflow context
-- | for weights initialization
instance (PureBuilder [Float] TFCtx (x # i),
          Inner x ~ Float,
          ExtractShape TFCtx (x # i # j)) => Builder TFCtx (x # i # j) where
  build = lift normalDistribution where
    normalDistribution :: [Float] = normals stdGen

-- | Session implementation for a float variable in the tensorflow context
instance WithSession TFCtx Float TF.Session (V.Vector Float) where
  session (TFCtx vec) = pure (V.fromList [vec])
  run = TF.runSession

-- | Session implementation for a float vector in the tensorflow context
instance WithSession TFCtx (Float # i) TF.Session (V.Vector Float) where
  session (TFCtx vec) = TF.run vec
  run = TF.runSession

-- | Session recursive implementation for a float tensor in the tensorflow context
instance {-# OVERLAPPABLE #-} WithSession TFCtx (x # i) TF.Session (V.Vector Float) => WithSession TFCtx (x # i # j) TF.Session (V.Vector Float) where
  session (TFCtx vec) = session (TFCtx @(x # i) vec)
  run = TF.runSession

-- | Implementation of training a dense model for the tensorflow backend
instance Trainable TFCtx (Float # m) i j (DenseParameters TFCtx Float i j) TF.Session where
  -- | return a session with updated weights by one backward pass
  trainStep mod lossFunc xData yData = result where
    result = do
      let ps = parameters mod
      let TFCtx w = weights ps
      let TFCtx b = bias ps

      w' <- TFV.initializedVariable w
      b' <- TFV.initializedVariable b

      let newPs = DenseParameters {
        weights = TFCtx (TFV.readValue w'),
        bias = TFCtx (TFV.readValue b')
      }

      let TFCtx loss = lossFunc (forward mod newPs xData) yData
      tStep <- TF.minimizeWith TF.adam loss [w', b']

      TF.run_ tStep
