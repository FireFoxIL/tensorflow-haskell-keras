module Dirk.TensorFlow.Core where

import Dirk.Data
import qualified TensorFlow.Core as TF (Tensor, Build)

-- | TensorFlow Context
-- | for building tensors for Tensorflow
type family CtxTF (a :: Type) where
  CtxTF () = ()
  CtxTF (b # i) = TF.Tensor TF.Build (Inner b)
  CtxTF Float = Float

-- | Synonim of the tensorflow context with an unwrapped tensor
newtype TFCtx a = TFCtx (CtxTF a)
