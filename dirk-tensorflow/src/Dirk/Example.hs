{-|
Module shows an example of using the framework to create a simple dense model
-}
module Dirk.Example where

import Dirk.Data
import Dirk.Layers
import Dirk.TensorFlow

import System.Random (randomIO)
import Control.Monad (replicateM, replicateM_)
import qualified Data.Vector as V

import qualified TensorFlow.Core as TF

model = dense @10 @10 @TFCtx @(Float # 10) @Float

appSession :: [Float] -> [Float] -> IO (V.Vector Float)
appSession xDataRaw yDataRaw = run @TFCtx @(Float # 1) @TF.Session $ do
  let xData = lift xDataRaw
  let yData = lift yDataRaw

  replicateM_ 100 (trainStep model mse xData yData)

  res <- inference model xData
  pure res

runApp = do
  xDataRaw :: [Float] <- replicateM 100 randomIO
  let yDataRaw :: [Float] = [x*3 + 8 | x <- xDataRaw]
  res <- appSession xDataRaw yDataRaw
  putStrLn (show yDataRaw)
  putStrLn (show res)
  let loss = sum (map abs (zipWith (-) (V.toList res) yDataRaw))
  putStrLn (show loss)
